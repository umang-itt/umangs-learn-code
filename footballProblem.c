#include <stdio.h>

void main()
{
    int testCase,passCount,initialPid,i,j,playerId,oldId,newId,tempId;
    char passType;

    //  No. of Test Cases
    scanf("%d",&testCase);
    for(i=0;i<testCase;i++){

	 // No. of passes and id
	scanf("%d %d", &passCount, &initialPid);
	
       newId=initialPid;

	for(j=0;j<passCount;j++){

	   // Enter the pass type P/B 
	   
	    scanf(" %c",&passType); 
	   // check pass type
		if(passType== 'p' || passType=='P'){
	   		
	   		// enter the player id whom you pass
	   		scanf("%d", &playerId);
	   		
	   	    oldId=newId;
			newId=playerId;	
	   			
	   }
	   else{
	   		tempId=newId;
			newId=oldId	;
			oldId=tempId;   	
	   }
	}
	printf("\n Player %d",newId);
}

}
